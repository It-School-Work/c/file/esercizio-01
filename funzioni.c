//
// Created by imadd on 18/03/2020.
//

#include <stdio.h>
#include <malloc.h>
#include "funzioni.h"

/**
 * @brief dato il nome di un file riempie una struttura di tipo vestiti contenente le righe lette dal file.
 * La funzione restituisce 1 se l'operazione è andata a buon fine, altrimenti restituisce 0.
 * @param struct vestiti
 * @param filename
 * @return bool
 */
bool leggi(struct vestiti v, char *filename) {
    FILE* fp = fopen(filename, "r");
    // check if the file exist
    if (!fp)
        return false;
    // check if the file contain dimension
    if (!fscanf(fp, "%d", &v.dimensione))
        return false;
    v.stock = malloc(sizeof(struct vestito) * v.dimensione);
    for (int i = 0; i < v.dimensione && !feof(fp); i++) {
        fscanf(fp, "%d %d %d %f", &v.stock[i].tipo, &v.stock[i].T, &v.stock[i].quantita, &v.stock[i].prezzo);
    }
    fclose(fp);
    return true;
}

/**
 * @brief filtra dalla struttura vestiti tutti i vestiti con taglia uguale al parametro taglia
 * e le copia nella struttua vestitiConTaglia
 * @param vestiti
 * @param vestitiConTaglia
 * @param taglia
 */
void estrai(struct vestiti v1, struct vestiti vestitiConTaglia, enum taglia T) {
    v1.dimensione = 0;
    int i, j;
    for (i = 0; i < v1.dimensione; i++) {
        if (v1.stock[i].T == T) {
            vestitiConTaglia.dimensione++;
        }
    }
    if (!vestitiConTaglia.dimensione)
        return;
    vestitiConTaglia.stock = malloc(sizeof(struct vestito) * vestitiConTaglia.dimensione);
    for (i = 0, j = 0; i < v1.dimensione; i++) {
        if (v1.stock[i].T == T) {
            vestitiConTaglia.stock[j].quantita = v1.stock[i].quantita;
            vestitiConTaglia.stock[j].T = v1.stock[i].T;
            vestitiConTaglia.stock[j].tipo = v1.stock[i].tipo;
            vestitiConTaglia.stock[j].prezzo = v1.stock[i].prezzo;
            j++;
        }
    }
}

/**
 * @brief calcola il numero totale di vestiti contenuti nella struttura vestiti
 * @return unsigned int
 */
unsigned int quant(struct vestiti v) {
    unsigned int quantitaTotale = 0, i = 0;
    for (; i < v.dimensione; i++) {
        quantitaTotale += v.stock[i].quantita;
    }
    return quantitaTotale;
}

/**
 * @brief calcola il prezzo totale dei vestiti contenuti nella struttura vestiti
 * @return float
 */
float prezzo(struct vestiti v) {
    float prezzoTotale = 0;
    int i = 0;
    for (; i < v.dimensione; i++) {
        prezzoTotale += v.stock[i].prezzo * ((float)(v.stock[i].quantita));
    }
    return prezzoTotale;
}

/**
 * @brief scrive in un file di testo, il cui nome è presenete nel parametro filename,
 * una riga per ogni taglia pari compresa fra il 42 e il 54
 * @param filename
 * @return bool
 */
bool scrivi(struct vestiti v, char* filename) {
    FILE *fp = fopen(filename, "w");
    if (!fp)
        return false;
    struct vestiti vestitiPerTaglia;
    unsigned int quantita;
    float prezzoTotale;
    for (enum taglia t = s; t <= xl; t += 2) {
        estrai(v, vestitiPerTaglia, t);
        quantita = quant(vestitiPerTaglia);
        prezzoTotale = prezzo(vestitiPerTaglia);
        fprintf(fp, "%d %d %d\n", t, quantita, prezzoTotale);
    }
    fclose(fp);
    return true;
}
