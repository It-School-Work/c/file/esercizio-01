//
// Created by imadd on 18/03/2020.
//

#ifndef ESERCIZIO_01_FUNZIONI_H
#define ESERCIZIO_01_FUNZIONI_H
#define maxRecords 20
//#define stockdata "vestiti.txt"
//#define fileTotali "totali.txt"

#include <stdbool.h>

enum taglia {
    s = 42,
    S = 44,
    m = 46,
    M = 48,
    l = 50,
    L = 52,
    xl = 54
};

struct vestito {
    unsigned int tipo;
    enum taglia T;
    unsigned int quantita;
    float prezzo;
};

struct vestiti {
    struct vestito *stock;
    unsigned int dimensione;
};

/**
 * @brief dato il nome di un file riempie una struttura di tipo vestiti contenente le righe lette dal file.
 * La funzione restituisce 1 se l'operazione è andata a buon fine, altrimenti restituisce 0.
 * @param struct vestiti
 * @param filename
 * @return bool
 */
bool leggi(struct vestiti v, char *filename);

/**
 * @brief filtra dalla struttura vestiti tutti i vestiti con taglia uguale al parametro taglia
 * e le copia nella struttua vestitiConTaglia
 * @param vestiti
 * @param vestitiConTaglia
 * @param taglia
 */
void estrai(struct vestiti v1, struct vestiti vestitiConTaglia, enum taglia T);

/**
 * @vrief calcola il numero totale di vestiti contenuti nella struttura vestiti
 * @return unsigned int
 */
unsigned int quant(struct vestiti v);

/**
 * @brief calcola il prezzo totale dei vestiti contenuti nella struttura vestiti
 * @return float
 */
float prezzo(struct vestiti v);

/**
 * @brief scrive in un file di testo, il cui nome è presenete nel parametro filename,
 * una riga per ogni taglia pari compresa fra il 42 e il 54
 * @param filename
 * @return bool
 */
bool scrivi(struct vestiti v, char* filename);

#endif //ESERCIZIO_01_FUNZIONI_H
